<?php
require_once('config.php');
$key = 'z2rydFNxfX';
$level = 3;
if(isset($_REQUEST['k']) &&  $_REQUEST['k']!='')
{
	$userKey = $_REQUEST['k'];
}else{
	$userKey = $key;
}

if(isset($userKey))
{	
	include('classes/network.class.php');
	$objMyNetwork = new MyNetwork($userKey,$level); 	
	$listArr = $objMyNetwork->MyNetwork($userKey,$level); 

?>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
  google.load('visualization', '1', {packages:['orgchart']});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Name');
	data.addColumn('string', 'Manager');
	data.addColumn('string', 'ToolTip');
	data.addRows([
	  <?php 
		$i=1;
		foreach($listArr as $lists=>$rowArr)
		{
			foreach($rowArr as $rows=>$row)
			{
				foreach($row as $rowDetail)
				{					
					if(empty($rowDetail['userKey']))
					{
						if($rowDetail['leg'] == 0){
							$userKey = $rowDetail['parentKey'].'_addl';
						}else{
							$userKey = $rowDetail['parentKey'].'_addr';
						}
					}else{
						$userKey = $rowDetail['userKey'];
					}	
					?>
					[{v:'<?= $userKey;?>', 
					
					f: '<div class="display"><span class="name"><?=$rowDetail['name']?></span><br><span class="userkey"><?=strtolower($rowDetail['userKey'])?></span><br\><span><?=$rowDetail['payment_status']?></span><br\><?php if(count($listArr)-1 == $lists && !empty($rowDetail['userKey']) ){?><span><a href="<?= '?k='.$rowDetail['userKey']?>">More</a></span><?php } ?></div>'},
					'<?=$rowDetail['parentKey']?>', null],
				
				<?php 
				}	
				
			}	
		}
	  ?>
	]);
	var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
	chart.draw(data, {allowHtml:true });
	
  }

  
</script>
<style type="text/css">
.display{
	padding:0px;
	margin:0px;
	width:auto;
	height:auto;
}
.display .name{
	font-weight:bold;
	line-height:22px;
}	
.display .userkey{
	font-weight:bold;
	margin:0 0 5px 0;
	color:#0000FF;
}	

	
</style>
	<div id='chart_div'></div>
	
<?php } else{ ?>
<div class="geneology error">error while generating the network. </div>

	
<?php } ?>